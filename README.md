# git-conflication

#### 介绍
git冲突管理练习

#### 安装说明（个人）

1.  安装[git bash](https://gitforwindows.org/)
2.  注册[gitee](https://gitee.com/)
3.  修改全局git配置
    ```
    git config --global -e
    ```
    ![avatar](resources/git_global_config.jpg)


#### 练习说明（小组）

1. 每个组的组长folk当前仓库， 点击右上角folk按钮（组长）![avatar](resources/how_to_folk.jpg)
2. 组长将folk后的仓库分享给每个组员（组长）![avatar](resources/repository_member_management.jpg)
3. 组里每个人都将组长folk后的仓库克隆到自己的本地（个人）![avatar](resources/how_to_clone.jpg)
   ```
   git clone [copied repository link]"
   ```
4. 修改team/team_member文件，在该文件中加上自己的名字（个人）
   ```
   git add .
   git commit -m "use your own message here"
   ```
5. 拉取远程仓库最新代码（个人）
    ```
    git pull -r
    ```
6. 如果没有冲突， 直接推送代码到远程分支。 注意：组里第一个提交代码的人不会遇到冲突， 如果想要进行冲突练习， 可以推送代码后等他人提交完再修改一下自己的名字，重复步骤4-6。 (个人)
   ```
    git push
    ```
   如果有冲突，先修改冲突文件,再推送代码到远程分支
    ```
    git add .
    git commit -m "resolve confict when merge"
    git pull -r
    git push
    ```


